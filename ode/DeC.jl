using Plots
using FastGaussQuadrature, Compat
using LinearAlgebra

"""Different models"""

include("Stability_model.jl")

function lagrange_basis(X,t,i)
    idxs = eachindex(X)
    if t in X
        if t == X[i]
            return 1
        else
            return 0
        end
    end
    return prod((t-X[j])/(X[i]-X[j]) for j in idxs if j != i)
end

            
function equispaced(order)
    nodes=range(-1,stop=1,length=order)
    w=zeros(order)
    for k=1:order
        zz=Poly([1.])
        for s=1:order
            if s == k
                continue
            else
                zz=zz* Poly([-nodes[s],1])
            end
        end
        zz=zz/polyval(zz,nodes[k])
        intzz=polyint(zz)
        w[k]=polyval(intzz,1)-polyval(intzz,-1)
    end
    return nodes, w
end
     

function get_nodes(order,nodes_type)
    if nodes_type=="equispaced"
        nodes, w = equispaced(order)
    elseif nodes_type=="gaussLobatto"
        nodes, w = gausslobatto(order)
    elseif nodes_type=="gaussLegendre"
        nodes, w = gausslegendre(order)
    end
    w = map( x-> x*0.5, w)
    nodes = nodes*0.5.+0.5    
    return nodes, w
end


function compute_theta_DeC(order, nodes_type)
    
    nodes, w = get_nodes(order,nodes_type)
    int_nodes, int_w = get_nodes(order,"gaussLobatto")
    # generate theta coefficients 
    theta = zeros(order,order)
    for m in 1:order
        nodes_m = int_nodes*(nodes[m])
        w_m = int_w*(nodes[m])
        for r in 1:order
            theta[r,m] = sum([lagrange_basis(nodes,nodes_m[k],r)*w_m[k] for k in 1:order])
        end
    end
    return theta
    
end


""" Computing the matrix of the values T_k^m = int_{t^0}^{t^m} varphi_k"""
function calculate_theta(M_sub::Int, distribution)
  if distribution=="equispaced"
    nodes=range(0,stop=1, length=M_sub+1)
  elseif distribution=="gaussLobatto"
    nodes,p = gausslobatto(M_sub+1)
    nodes=nodes*0.5.+0.5
  end
  theta=compute_theta(nodes)
  return theta
end


#Dec_correction-
#Func is the function of the ode
#N_step -> Number of subtimesteps
#Corr-> Number of Corrections
#y_0 Initial condition
# t_0 Start value
# t_end End value
# Maybe logical variable for checking for the different solvers or the distributaiton
# of subtimesteps up to some order
#Distribution is set equidistand or different order

""" Dec_correction constant time step"""
function dec_Remi(func, tspan, y_0, M_sub::Int, K_corr::Int, distribution)

   N_time=length(tspan)
   dim=length(y_0)
   U=zeros(dim, N_time)
   u_p=zeros(dim, M_sub+1)
   u_a=zeros(dim, M_sub+1)
   u_help=zeros(dim)
   func_corr=zeros(dim,M_sub+1)
   Theta = compute_theta_DeC(M_sub+1,distribution)
   U[:,1]=y_0
   for it=2: N_time
        delta_t=(tspan[it]-tspan[it-1])
        for m=1:M_sub+1
         u_a[:,m]=U[:,it-1]
         u_p[:,m]=U[:,it-1]
        end
        for k=2:K_corr+1
            u_p=copy(u_a)
            for r=1:M_sub+1
                func_corr[:,r]=func(u_p[:,r])
            end
            for m=2:M_sub+1
            u_a[:,m]= u_a[:,1]+delta_t*sum(Theta[r,m]*func_corr[:,r] for r in 1:M_sub+1)
            end
        end
        U[:,it]=u_a[:,M_sub+1]
    end
    return tspan, U
end

function dec_Remi_Stab(func,  y_0,a:: ComplexF64, M_sub::Int, K_corr::Int,distribution)

   #N_time=length(tspan)
   dim=length(y_0)
   U=zeros(ComplexF64,dim,2)
   u_p=zeros(ComplexF64, dim, M_sub+1)
   u_a=zeros(ComplexF64, dim, M_sub+1)
   func_corr=zeros(ComplexF64,dim,M_sub+1)
   Theta = compute_theta_DeC(M_sub+1,distribution)
   U[:,1]=y_0
   delta_t=1
   for it=2: 2
        for m=1:M_sub+1
         u_a[:,m]=U[:,it-1]
         u_p[:,m]=U[:,it-1]
        end
        for k=2:K_corr+1
            u_p=copy(u_a)
            for r=1:M_sub+1
                func_corr[:,r]=func(u_p[:,r],a)
            end
            for m=2:M_sub+1
            u_a[:,m]= u_a[:,1]+delta_t*sum(Theta[r,m]*func_corr[:,r] for r in 1:M_sub+1)
            end
        end
        U[:,it]=u_a[:,M_sub+1]
    end
    return U
end


