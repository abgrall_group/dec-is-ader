using Plots
using FastGaussQuadrature
using Polynomials

function lagrange_basis(X,t,i)
    idxs = eachindex(X)
    if t in X
        if t == X[i]
            return 1
        else
            return 0
        end
    end
    return prod((t-X[j])/(X[i]-X[j]) for j in idxs if j != i)
end

function lagrange_deriv(X,t,i)
    idxs = eachindex(X)
    f = 0
    for k in idxs
        p = 1
        if i == k
            continue
        end
        for j in idxs
            if j != i && j !=k
                p = p*(t-X[j])/(X[i]-X[j])
            end
        end
        f = f + p/(X[i]-X[k])
    end
    return f
    
end

function equispaced(order)
    nodes=range(-1,stop=1,length=order)
    quad_nodes, quad_w = gausslegendre(order)
    w=zeros(order)
    for k=1:order
        w[k]=0.
        for s=1:order
            w[k]=w[k]+lagrange_basis(nodes,quad_nodes[s],k)*quad_w[s]
        end
    end
    return nodes, w
end
            
function get_nodes(order,nodes_type)
    if nodes_type=="equispaced"
        nodes, w = equispaced(order)
    elseif nodes_type=="gaussLobatto"
        nodes, w = gausslobatto(order)
    elseif nodes_type=="gaussLegendre"
        nodes, w = gausslegendre(order)
    end
    w = map( x-> x*0.5, w)
    nodes = nodes*0.5.+0.5    
    return nodes, w
end

# generate ADER matrix
function get_ADER_matrix(order, nodes_type)
    
    nodes_poly, w_poly = get_nodes(order,nodes_type)
    if nodes_type=="equispaced"
        quad_order=order
        nodes_quad, w = get_nodes(quad_order,"gaussLegendre")
    else
        quad_order=order
        nodes_quad, w = get_nodes(quad_order,nodes_type)
    end
                    
    # generate mass matrix
    M = zeros(order,order)
    for i in 1:order
        for j in 1:order
            M[i,j] = lagrange_basis(nodes_poly,1.0,i)*lagrange_basis(nodes_poly,1.0,j)-sum([lagrange_deriv(nodes_poly,nodes_quad[q],i)*lagrange_basis(nodes_poly,nodes_quad[q],j)*w[q] for q=1:quad_order])
        end
    end
    # generate mass matrix
    RHSmat = zeros(order,order)
    for i in 1:order
        for j in 1:order
            RHSmat[i,j] = sum([lagrange_basis(nodes_poly,nodes_quad[q],i)*lagrange_basis(nodes_poly,nodes_quad[q],j)*w[q] for q=1:quad_order])
        end
    end
    return nodes_poly, w_poly, M, RHSmat
    
end

function ADER_ODE(func, ts, y_0, M_sub::Int, K_corr::Int, distribution)
    # Ts: list of timesteps linspace(0,Tend)
   Nsteps = length(ts)
   nDim = length(y_0) # dimension of system
    
   U = zeros(nDim, Nsteps) # full solution
   u_p = zeros(nDim, M_sub+1) # substep sols
   u_a = zeros(nDim, M_sub+1)
   u_tn = zeros(nDim, M_sub+1)
   func_corr = zeros(nDim,M_sub+1)
   rhs=zeros(nDim,M_sub+1)
    
   x_poly, w_poly, ADER, RHS_mat = get_ADER_matrix(M_sub+1, distribution)
   invader = inv(ADER)
    
   # initialise U
   U[:,1] = y_0
    
   for it = 2: Nsteps
        delta_t = (ts[it]-ts[it-1])
        for m = 1:M_sub+1 # initialise substep sols
         u_a[:,m] = U[:,it-1]
         u_p[:,m] = U[:,it-1]
         u_tn[:,m] = U[:,it-1]
        end
        
        for k = 2:K_corr+1
            u_p = copy(u_a)
            # multi-d
            for r=1:M_sub+1
                rhs[:,r]=func(u_p[:,r])
            end
            for dim = 1:nDim
                u_a[dim,:]= u_tn[dim,:]+delta_t*invader*RHS_mat*rhs[dim,:]
            end

        end
        
        for dim = 1:nDim
            U[dim,it] = sum(u_a[dim,:].*[lagrange_basis(x_poly,1.0,i) for i in 1:M_sub+1])
        end

    end
    return ts, U
end
        

function stab_func_complex(u,lambda:: ComplexF64)
    p=length(u)
    d=zeros(ComplexF64,p)
    d[1]=lambda*u[1]
    return  d
end

function ADER_ODE_stab(func, y_0,lambda:: ComplexF64, M_sub::Int, K_corr::Int,distribution)
   """ Modified ADER, accepting a complex value"""
   nDim=length(y_0)
   U=zeros(ComplexF64,nDim,2)
   u_p=zeros(ComplexF64, nDim, M_sub+1)
   u_a=zeros(ComplexF64, nDim, M_sub+1)
   u_tn=zeros(ComplexF64, nDim, M_sub+1)
   func_corr=zeros(ComplexF64,nDim,M_sub+1)
            
   rhs=zeros(ComplexF64,nDim,M_sub+1)
   
   x, w, ADER, RHS_mat = get_ADER_matrix(M_sub+1, distribution)
   invader = inv(ADER)
   
   U[:,1]=y_0
   delta_t=1
   # Just one iteration 
   for it=2: 2
        for m = 1:M_sub+1 # initialise substep sols
         u_a[:,m] = U[:,it-1]
         u_p[:,m] = U[:,it-1]
         u_tn[:,m] = U[:,it-1]
        end
        
        for k = 2:K_corr+1
            u_p = copy(u_a)
                    
            for r=1:M_sub+1
                rhs[:,r]=func(u_p[:,r],lambda)
            end

            for dim = 1:nDim
                temp = delta_t*invader*RHS_mat*rhs[dim,:]
                t4 = [a[1] for a in temp]
                t2 = vec(u_tn[dim,:])
                u_a[dim,:]= t2+t4
            end
        end
        
        for dim = 1:nDim
            U[dim,it] = sum(u_a[dim,:].*[lagrange_basis(x,1.0,i) for i in 1:M_sub+1])
        end
        
    end
    
    return U
    
end
